<?php
 
/**
*
* Plugin Name: accordion menu webk
* Plugin URI: http://www.webkreativo.com
* Description: permite crear menus desplegables
* Version: 1.0
* Author: webkreativo
* Author URI: http://www.webkreativo.com
*
**/

/*Actions Agregados*/
add_action('admin_menu','accordion_sidebar_menu');
add_action('wp_footer', 'webk_accordion_footer');
add_action('wp_head', 'webk_accordion_header');
/*-----------------*/

function my_plugin_admin_init() {
   	/* Register our stylesheet. */
	wp_register_style( 'myPluginStylesheet', plugins_url('css/webk-accordionmenu.css', __FILE__) );

	register_setting( 'accordionmenu_group', 'menu_id', 'webk_menus_validator');
	register_setting( 'accordionmenu_group', 'number_items', 'webk_first_validator');

	add_settings_section('webksettings_section_id', 'Accordion Menu Settings', 'webksettings_section_description', 'webk-accordionmenu');

	add_settings_field('webk_menus_id', 'Select Sidebar Menu:', 'webk_menus_display', 'webk-accordionmenu', 'webksettings_section_id');
	add_settings_field('webk_first_num_id', 'Do this for the first:', 'webk_first_num_display', 'webk-accordionmenu', 'webksettings_section_id');

}

function webksettings_section_description() {
	echo '<p>Main description of this section here.</p>';
}

function webk_menus_display() {
	$options = get_option('menu_id');

	$menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );

	/*echo "<pre>";
	var_dump($menus);
	echo "</pre>";*/

	echo '<select id="webk_menus_id" name="menu_id[webk_menus_id]" >';
	echo '<option value="'.($options['webk_menus_id'] == '' ? 'selected' : '' ).'"> -- </option>';
	foreach ($menus as $menu) {
		echo '<option value='.$menu->slug.' '.($menu->slug == $options["webk_menus_id"] ? 'selected' : '').'>'.$menu->name.'</option>';
	}
	echo '</select>';
	
	/*echo "<input id='webk_menus_id' name='menu_id[webk_menus_id]' size='40' type='text' value='{$options["webk_menus_id"]}' />";*/
}

function webk_first_num_display() {
	$options = get_option('number_items');
	
	echo "<input id='webk_first_num_id' name='number_items[webk_first_num_id]' size='40' type='numeric' value='{$options["webk_first_num_id"]}' />";
}

function webk_menus_validator($input) {
	
	$newinput['webk_menus_id'] = trim($input['webk_menus_id']);
	
	return $newinput;
}

function webk_first_validator($input) {

	/*die(var_dump($input));*/

	$newinput['webk_first_num_id'] = trim($input['webk_first_num_id']);

	if (!is_numeric($newinput['webk_first_num_id'])) {
		return $newinput['webk_first_num_id'] = $input['webk_first_num_id'] = 0;
	}
	
	return $newinput;
}


function accordion_sidebar_menu()
{
	$webkpage = add_menu_page('Settings', 'Accordion menu', 'administrator', 'webk-accordionmenu', 'show_menu_page');
	add_action( 'admin_print_styles-' . $webkpage, 'my_plugin_admin_styles' );
	add_action( 'admin_init', 'my_plugin_admin_init' );
}

function my_plugin_admin_styles() {
   /*
    * It will be called only on your plugin admin page, enqueue our stylesheet here
    */
   wp_enqueue_style( 'myPluginStylesheet' );
}




function show_menu_page()
{
	/*$menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );*/ /*para obtener todos los menús creados*/
?>
	<!--[if gte IE 9]>
		<style type="text/css">
			.gradient {
				filter: none !important;
			}
		</style>
	<![endif]-->

	<div id="webk-accordionmenu" class="gradient">
		<form action="options.php" method="post">
			<?php settings_fields( 'accordionmenu_group' ); ?>
			<div class="webk-rows">
				<?php do_settings_sections('webk-accordionmenu'); ?>
			</div>
			<?php submit_button(); ?>
		</form>
	</div>

<?php



					
}

function webk_accordion_footer() {
	/*$options = get_option('menu_id');*/
	$options = get_option('menu_id');
	$num = get_option('number_items');
	/*echo "<pre>";
	var_dump($num);
	echo "</pre>";*/
	/*echo "<pre>";
	var_dump($options);
	echo "</pre>";
	echo '<p>'.$options["webk_menus_id"].'</p>';*/
?>

	<script type="text/javascript">
		var menuname = "<?php echo $options["webk_menus_id"]; ?>";
		var menunumber = "<?php echo ( (int)$num["webk_first_num_id"] == 0 ? 0 : (int)$num["webk_first_num_id"] ); ?>";
		console.log(menunumber);

		jQuery(document).ready(function () {
			if ( jQuery('.widget .menu-'+menuname+'-container').length > 0 ) {
				if ( jQuery('.widget .menu-'+menuname+'-container .menu').length > 0 ){
					jQuery('.widget .menu-'+menuname+'-container .menu ul.sub-menu').each(function () {
						jQuery(this).fadeOut();
						jQuery(this).parent('li').addClass('has_aSon');
						jQuery(this).parent('li').prepend('<div class="webk_symbol has_aSon_plus"></div>');
					});
				}
			};

			jQuery('li.has_aSon div.webk_symbol').unbind('click').toggle(function () {

				jQuery(this).parent().children('.webk_symbol').removeClass('has_aSon_plus');

				jQuery(this).parent().children('.webk_symbol').addClass('has_aSon_minus');

				jQuery(this).parent().children('ul.sub-menu').slideDown();

				jQuery(this).parent().addClass('no_borders');
			 
		    },function () {

		    	jQuery(this).parent().children('.webk_symbol').removeClass('has_aSon_minus');
			 
		    	jQuery(this).parent().children('.webk_symbol').addClass('has_aSon_plus');

		        jQuery(this).parent().children('ul.sub-menu').slideUp();
			 
				jQuery(this).parent().removeClass('no_borders');
			    
			});

			if (menunumber > 0) {

				jQuery('li.has_aSon div.webk_symbol').each(function () {
					if (menunumber != 0 ) {
						jQuery(this).trigger('click');
						menunumber--;
					}else{
						return false;
					}
				});
			};


			

		});

		/*jQuery(window).load(function () {
		});*/
	</script>
<?php
	/*echo '<p>test</p>';*/
}


function webk_accordion_header()
{
?>

	<style type="text/css">
		ul.sub-menu{
			padding-left: 15px;
		}

		li.has_aSon{
			/*padding-left: 20px;*/
			/*width: 15px; 
			height: 15px;*/
		}

		div.webk_symbol{
			margin-right: 5px;
		}

		div.has_aSon_plus{
			background: url('<?php echo plugins_url( "plus.png", __FILE__ ); ?>');
			background-repeat: no-repeat;
			background-position: -20px -35px;
			display: inline-block;
			*display: inline;
			height: 15px;
			vertical-align: middle;
			width: 15px; 
			zoom:1;

			-moz-transition: all 0.2s linear 0s;
			-webkit-transition: all 0.2s linear 0s;
			-o-transition: all 0.2s linear 0s;
			-ms-transition: all 0.2s linear 0s;
			transition: all 0.2s linear 0s;
		}

		div.has_aSon_plus:hover{
			
			background-position: -20px -0px;
			-moz-transition: all 0.2s linear 0s;
			-webkit-transition: all 0.2s linear 0s;
			-o-transition: all 0.2s linear 0s;
			-ms-transition: all 0.2s linear 0s;
			transition: all 0.2s linear 0s;

		}

		div.has_aSon_minus{
			background: url('<?php echo plugins_url( "minus.png", __FILE__ ); ?>');
			background-repeat: no-repeat;
			background-position: -20px -0px;
			display: inline-block;
			*display: inline;
			height: 15px;
			vertical-align: middle;
			width: 15px; 
			zoom:1;

			-moz-transition: all 0.2s linear 0s;
			-webkit-transition: all 0.2s linear 0s;
			-o-transition: all 0.2s linear 0s;
			-ms-transition: all 0.2s linear 0s;
			transition: all 0.2s linear 0s;
		}

		div.has_aSon_minus:hover{
			background-position: -20px -35px;

			-moz-transition: all 0.2s linear 0s;
			-webkit-transition: all 0.2s linear 0s;
			-o-transition: all 0.2s linear 0s;
			-ms-transition: all 0.2s linear 0s;
			transition: all 0.2s linear 0s;
		}

		li.no_borders{
			border: none;
		}

	</style>
<?php
		/*plugin_dir_path( _FILE_ )
		plugins_url( __FILE__ );
		#D8A528
		*/
}